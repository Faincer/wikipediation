package main

import (
	"fmt"
	"gitlab.com/Faincer/wikipediation"
	"github.com/vitmovie/articlesgo"
	"bitbucket.org/rumbletomato/cla_car"
)

func TestInsertArticleLinkInText() {
	var ConfluenceID int32 = 32980
	temp_text, _  := cla_car.NewCLACar(Login, Token, Domain).GetArticleBody(ConfluenceID)
	links := []goarticles.Article{
		goarticles.Article{Title: "Linkerbot", ConfluenceID: 32980, Url: "https://linkerbot.atlassian.net/"},
	}

	text := wikipediation.InsertArticleLinkInText(links, temp_text)

	fmt.Println("Text before: ", temp_text)
	fmt.Println("Text after: ", text)
}

func TestWikipediation() {
	var ConfluenceID int32 = 32980
	temp_text, _  := cla_car.NewCLACar(Login, Token, Domain).GetArticleBody(ConfluenceID)
	db := goarticles.NewDatabase(username, password, host, dbname)

	text := wikipediation.Wikipediation(db, temp_text)

	fmt.Println("Text before: ", temp_text)
	fmt.Println("Text after: ", text)
}

func main() {
	TestInsertArticleLinkInText()
	TestWikipediation()
}
