module gitlab.com/Faincer/wikipediation

go 1.15

require (
	bitbucket.org/rumbletomato/cla_car v1.0.0
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/vitmovie/articlesgo v1.5.0
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc
)
