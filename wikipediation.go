package wikipediation

import (
	"strings"
	"github.com/vitmovie/articlesgo"
	"golang.org/x/net/html"
)


func getArticleData(DB *goarticles.Database) []goarticles.Article {
	var articles []goarticles.Article
	article := goarticles.Article{DB: DB}
	articles, _ = article.GetAllArticles()
	return articles
}


func insert(article goarticles.Article) string {
	return "<a href='"+article.Url+"'>"+article.Title+"</a>"
}


// InsertArticleLinkInText - insert links in input text using article-data 
func InsertArticleLinkInText(articles []goarticles.Article, text string) string {
	textReader := strings.NewReader(text)
	z := html.NewTokenizer(textReader)
	
	var isAnchor bool = false
	var sliceWords []string
	for {
		tt := z.Next()
		if tt == html.ErrorToken {
			break
		}

		t := z.Token()
		tString := t.String()
		switch {
		case tt == html.StartTagToken:
			isAnchor = t.Data == "a"
		case tt == html.EndTagToken:
			isAnchor = !(t.Data == "a")
		case tt == html.TextToken:
			if !isAnchor {
				for _, link := range articles {
					tString = strings.ReplaceAll(tString, link.Title, insert(link))
				}
			}
		}		
		sliceWords = append(sliceWords, tString)
	}
	return strings.Join(sliceWords, "")
}


// Wikipediation - get article-data and generate new text with links using input text
func Wikipediation(database *goarticles.Database, text string) string {
	var articles = getArticleData(database)
	textWithLinks := InsertArticleLinkInText(articles, text)
	return textWithLinks
}
